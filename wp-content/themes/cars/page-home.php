<?php get_header(); ?>
	<main>
		<?php get_template_part( 'template-parts/pages/content', 'title' ); ?>
		<?php get_template_part( 'template-parts/pages/content', 'excerpt' ); ?>
		<?php get_template_part( 'template-parts/pages/content', 'post-detail' ); ?>	
	</main>
<?php get_footer(); ?>	
