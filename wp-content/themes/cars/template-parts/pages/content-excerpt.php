<section class="excerpt">
	<div class="wrapper">
		<h2>Task Two : Display the excerpt for the posts D and C. (Excerpt length = 50)</h2>
		<?php 
			$allposts = new WP_Query(array('post_type' => 'cars'));
			
			if ($allposts->have_posts()) : ?>
				<ul>
					<?php while ($allposts->have_posts()) :
						$allposts->the_post();
						if (in_category('grade_c') || in_category('grade_d')) : ?>
							<li>
								<p><?php echo custom_excerpt(); ?></p>
							</li>
						<?php endif; ?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>	
	</div>			
</section>