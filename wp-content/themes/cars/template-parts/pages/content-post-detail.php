<section class="post-detail">
	<div class="wrapper">
		<h2>Task Three : Create list of posts with Category A (list should display 'Featured Image', 'Date', 'Title', 'Excerpt', 'Read More')</h2>
		<?php 
			$allposts = new WP_Query(array('post_type' => 'cars'));
			
			if ($allposts->have_posts()) : ?>
				<ul>
					<?php while ($allposts->have_posts()) :
						$allposts->the_post(); ?>
						<?php if (in_category('grade_a')): ?>
							<li>
								<figure>
									<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>" alt="Post Image">
								</figure>
								<span><?php echo get_the_date(); ?></span>
								<h3><?php the_title(); ?></h3>
								<p><?php echo custom_excerpt(); ?></p>
								<a href="<?php the_permalink(); ?>" title="Read More">Read More</a>
							</li>
						<?php endif; ?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>	
	</div>				
</section>