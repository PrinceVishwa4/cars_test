<section class="particularPostDescription">
	<div class="single-post">
	  	<?php if (have_posts()) : ?>
			<figure>
				<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>" alt="Post Image">
			</figure>
			<h2><?php the_title(); ?></h2>
			<p><?php the_field('description'); ?></p>
		<?php endif; ?>
	</div>

	<div class="random-post">
		<div class="wrapper">
			<?php 
				$randompost = new WP_Query(array('post_type' => 'cars', 'orderby' => 'rand', 'showposts' => '3'));

				if ($randompost->have_posts()) : ?>
					<ul>
						<?php while ($randompost->have_posts()) : $randompost->the_post(); ?>
							<li>
								<figure>
									<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>" alt="Post Image">
								</figure>
								<h2><?php the_title(); ?></h2>
								<p><?php echo custom_excerpt(); ?></p>
								<a href="<?php the_permalink(); ?>" title="Read More">Read More</a>
							</li>
						<?php endwhile; ?>	
					</ul>
				<?php endif; ?>	
		</div>		
	</div>
</section>