<section class="title">
	<div class="wrapper">
		<h2>Task One : Display the list of titles for posts with categories A, B and C (title should not be repeated)</h2>
		<?php 
			$allposts = new WP_Query(array('post_type' => 'cars', 'order' => 'ASC'));
			
			if ($allposts->have_posts()) : ?>
				<ul>
					<?php while ($allposts->have_posts()) :
						$allposts->the_post();
						if (in_category('grade_a') || in_category('grade_b') || in_category('grade_c')) : ?>
							<li>
								<p><?php the_title(); ?></p>
							</li>
						<?php endif; ?>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
	</div>				
</section>