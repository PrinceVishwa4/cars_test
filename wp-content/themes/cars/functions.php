<?php

	add_theme_support( 'post-thumbnails' );
	get_post_type_labels(Posts’ / ‘Pages’);

	// Enqueing Style Sheet
	function stylesheet() 
	{
		wp_register_style('style',get_template_directory_uri() . '/style.css', array(), false, 'all');
		wp_enqueue_style('style');
	}
	add_action( 'wp_enqueue_scripts', 'stylesheet' );

	// Displaying Navigation Menu
	function load_navigation_menu()
	{
		register_nav_menu('primary', 'Top Navigation');
	}
	add_action('init', 'load_navigation_menu');

	// Custom Post
	function custom_posttype() 
	{
	  	register_post_type( 'cars',
		    array(
		      	'labels' => array(
		          	'name' => __( 'Cars' ),
		          	'singular_name' => __( 'Cars' ),
		          	'add_new'       => __('Add New'),
		          	'all_items'     => __('All Items'),
		          	'add_new_item'  => __('Add Item'),
		          	'edit_item'     => __('Edit Item'),
		          	'new_item'      => __('New Item'),
		          	'view_item'     => __('View Item'),
		          	'featured_image'        => __('featured_image'),
		          	'set_featured_image'    => __('set_featured_image'),
		          	'remove_featured_image' => __('remove_featured_image'),
		          	'use_featured_image'    => __('use_featured_image')
		      	),
		      	'public' => true,
		      	'has_archive' => true,
	      		'rewrite' => array('slug' => 'cars'),
		      	'supports' => array('title', 'editor', 'thumbnail'),
		      	'taxonomies'  => array( 'category' ),
		    )
	  	);
	}
	add_action( 'init', 'custom_posttype' );

	// Removing Default WYSIWYG Editor
	add_action( 'init', function() {
  		remove_post_type_support( 'cars', 'editor' );
	}, 99);

	// Custom Excerpt
	function custom_excerpt() 
	{
		global $post;
		$description = get_field('description');
		if ($description != '') {
			$description = strip_shortcodes($description);
			$description = apply_filters('the_content', $description);
			$description = str_replace(']]>', ']]>', $description);
			$excerpt_length = 50;
			$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
			$description = wp_trim_words($description, $excerpt_length, $excerpt_more);
		}
		return apply_filters('the_excerpt', $description);
	}

 ?>