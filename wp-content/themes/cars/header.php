<!DOCTYPE html>
<html>
	<head>
		<title>Cars</title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
		<!-- Header Start Here -->
		<header>
			<!-- Wrapper Starts Here -->
			<div class="wrapper">
				<h1>
					<a href="index.php" title="Cars">Logo</a>
				</h1>
				<nav>
					<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
				</nav>
			</div>
			<!-- Wrapper Ends Here -->
		</header>
		<!-- Header End Here -->